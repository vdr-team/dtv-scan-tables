#!/bin/sh

set -e

GIT=git://linuxtv.org/dtv-scan-tables.git

echo Downloading latest snapshot from git repository...

DEB_SOURCE_PACKAGE=`egrep '^Source: ' debian/control | cut -f 2 -d ' '`
VERSION_UPSTREAM=`dpkg-parsechangelog | grep ^Version: | sed -e 's/^Version:\s*//' -e s/-[^-]*$// -e s/\+git.*//`

git clone --depth 1 ${GIT}

GIT_SHA=`git --git-dir="${DEB_SOURCE_PACKAGE}/.git" show --pretty=format:"%h" --quiet | head -1 || true`

VERSION_DATE=`git --git-dir="${DEB_SOURCE_PACKAGE}/.git" log --no-color -1 --date=iso | sed -ne "s/Date:\s\+\(.*\).*/\1/p" | cut -d" " -f1 | tr -d "-"`
VERSION_FULL="${VERSION_UPSTREAM}+git${VERSION_DATE}.${GIT_SHA}"
TARBALL="../${DEB_SOURCE_PACKAGE}_${VERSION_FULL}.orig.tar.gz"

tar --exclude-vcs -c ${DEB_SOURCE_PACKAGE} | gzip -9 >"${TARBALL}"

rm -rf ${DEB_SOURCE_PACKAGE}

if [ -d .git ]; then
  read -p "Import '${TARBALL}' into Git (y/N)?" ANSWER
  case ${ANSWER} in
    [Yy] )
       gbp import-orig --pristine-tar "${TARBALL}"
       dch -v "${VERSION_FULL}-1" "New Upstream Snapshot (commit ${GIT_SHA})"
      break
      ;;
  esac
fi
